
#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = x + xc;
    new_y = y + yc;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    //7 points
	new_x = y + xc;
	new_y = x + yc;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = -x + xc;
	new_y = -y + yc;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = -y + xc;
	new_y = -x + yc;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = -x + xc;
	new_y =  y + yc;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = -y + xc;
	new_y =  x + yc;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = x + xc;
	new_y = -y + yc;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = y + xc;
	new_y = -x + yc;
	SDL_RenderDrawPoint(ren, new_x, new_y);

}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	int p = 3 - 2 * R;
	while (x <= y) {
		if (p < 0)
			p += 4 * x - 6;
		else {
			y--;
			p += 4 * (x - y )+ 10;
		}
		Draw8Points(xc, yc, x, y, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	float P = 1 - R;
	Draw8Points(xc, yc, x, y, ren);
	while (x <= y) {
		if (P < 0)
			P += (x<<1)+3;
		else {
			y--;
			P += ((x-y)<<1)+5;
		}
		Draw8Points(xc, yc, x, y, ren);
		x++;
	}
}
