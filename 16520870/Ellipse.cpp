#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x+xc, y+yc);
	SDL_RenderDrawPoint(ren, -x+xc, y+yc);
	SDL_RenderDrawPoint(ren, -x+xc, -y+yc);
	SDL_RenderDrawPoint(ren, x+xc,-y+yc);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	Draw4Points(xc, yc, 0, a, ren);
    // Area 1
	int x = a;
	int y = b; 
	int c = -2 * b*b*a + b * b + 2 * a * a;
	int d = 4 * b*b + 6 * a*a;
	for (; (b*b)*(b*b) >= y*y*(b*b + a*a); y++) {
		if (c <= 0)
			c += 4 * a*a*y + 6 * a*a;
		else {
			c += 4 * a*a*y - 4 * b*b*x + d;
			x--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	Draw4Points(xc, yc, 0, b, ren);
    // Area 2
	x = 0;
	y = b;
	c = -2 * a*a*b + a * a + 2 * b*b;
	d = 4 * a*a + 6 * b*b;
	for (; (a*a)*(a*a) >= x * x*(a*a + b * b); x++)
	{
		if (c <= 0)
			c += 4 * b*b*x + 6 * b*b;
		else
		{
			c += 4 * b*b*x - 4 * a*a*y + d;
			y--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x, y;
	float d1, d2;
	x = 0;
	y = b;
	d1 = b*b - a*a*b + a*a / 4;
	Draw4Points(xc,yc,x,y,ren);
    // Area 1
	while (a*a*(y - 1 / 2) > b*b*(x + 1)) {
		if (d1 < 0) {
			d1 +=  b*b*(2 * x + 3);
			++x;

		}
		else {
			d1 +=b*b*(2 * x + 3) + a*a*((-2) * y + 2);
			++x; --y;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	d2 = b*b*(x + 1/2)*(x + 1/2) + a*a*(y - 1)*(y - 1) - a*a*b*b;

    // Area 2
	while (y > 0) {
		if (d2 < 0) {
			d2 += b*b*(2 * x + 2) + a*a*((-2) * y + 3);
			++x; --y;
		}
		else {
			d2 +=  a*a*((-2) * y + 3);
			--y;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}