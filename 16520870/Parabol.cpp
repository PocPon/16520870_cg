#include "Parabol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
}
void BresenhamDrawParabolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	if (A > 0)
		A = -A;
	int a, b;
	SDL_GetRendererOutputSize(ren, &a, &b);
	int x = 0;
	int y = 0; 
	int c = A;
	Draw2Points(xc, yc, x, y, ren);
	for (; x <= -A; x++)
	{
		if (c > 0)
			c -= 2 * x + 1; 
		else
		{
			c -= 2 * x + 1 - 2 * (-A);
			y--;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
	c = 4 * A*y - 2 * x*x - 2 * x - 1;
	Draw2Points(xc, yc, x, y, ren);
	for (; y + yc>0; y--)
	{
		if (c < 0)
			c += -4 * A;
		else
		{
			c += -4 * A - 4 * x - 4;
			x++;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParabolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	if (A < 0)
		A = -A;
	int a, b;
	SDL_GetRendererOutputSize(ren, &a, &b);
	int x = 0;
	int y = 0; 
	int c = -A;
	Draw2Points(xc, yc, x, y, ren);
	for (; x <= A; x++)
	{
		if (c < 0)
			c += 2 * x + 1;
		else
		{
			c += 2 * x + 1 - 2 * A;
			y++;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
	c = 4 * A*y - 2 * x*x - 2 * x - 1;
	Draw2Points(xc, yc, x, y, ren);
	for (; y + yc <= b; y++)
	{
		if (c < 0)
			c += 4 * A; 
		else
		{
			c += 4 * A - 4 * x - 4;
			x++;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
}