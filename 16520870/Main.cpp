#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;




int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);
	

	//YOU CAN INSERT CODE FOR TESTING HERE
	SDL_Color color;
	color.r = 128;
	color.g = 0;
	color.b = 128;
	color.a = 0;

	Vector2D v1(200, 200);
	Vector2D v2(300, 200);
	Vector2D v3(400, 450);
	//TriangleFill(v1, v2, v3, ren, color);
	//FillIntersectionRectangleCircle(v1, v3, 300, 400, 50, ren, color);
	//RectangleFill(v1, v3, ren, color);
	//CircleFill(200, 200, 100, ren, color);
	//FillIntersectionTwoCircles(100, 200, 100, 150, 100, 90, ren, color);
	//FillIntersectionEllipseCircle(250, 400, 300, 200, 400, 400, 200, ren, color);
	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	
	bool running = true;

	SDL_Rect rect[4];
	rect[0] = { 200,200, 8,8 };
	rect[1] = { 300,100, 8,8 };
	rect[2] = { 350,400, 8,8 };
	rect[3] = { 100,400, 8,8 };
	int Choose = 0, Motion = 0;
	SDL_Delay(2000);

	while (running)
	{    
		SDL_Event E;
		//If there's events to handle
		if (SDL_PollEvent(&E))
		{
			int x = 0, y = 0;
			SDL_GetMouseState(&x, &y);
			if (E.type == SDL_MOUSEBUTTONDOWN && E.button.button == SDL_BUTTON_LEFT)
				for (int j = 0; j <= 3; j++)
					if (x >= rect[j].x&&y >= rect[j].y&&x <= rect[j].x + rect[j].w&&y < rect[j].y + rect[j].h)
					{
						Motion = 1;
						Choose = j;
					}
			if (E.type == SDL_MOUSEBUTTONUP && E.button.button == SDL_BUTTON_LEFT)Motion = 0;
			if (Motion)
			{
				rect[Choose].x = x;
				rect[Choose].y = y;
			}

			//If the user has Xed out the window
			if (E.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}
		// background
		SDL_SetRenderDrawColor(ren,0, 0, 255, 0);
		SDL_RenderClear(ren);
		// lines
		SDL_SetRenderDrawColor(ren, 239, 128, 107, 255);
		for (int i = 0; i < 3; i++)DDA_Line(rect[i].x, rect[i].y, rect[i + 1].x, rect[i + 1].y, ren);
		// curve
		SDL_SetRenderDrawColor(ren, 169, 0, 0, 255); 
		Vector2D v(rect[0].x, rect[0].y);
		DrawCurve3(ren, Vector2D(rect[0].x, rect[0].y),
			Vector2D(rect[1].x, rect[1].y), Vector2D(rect[2].x, rect[2].y), Vector2D(rect[3].x, rect[3].y));
		// rects
		SDL_SetRenderDrawColor(ren, 128, 244, 0, 181); 
		for (int i = 0; i < 4; i++)SDL_RenderFillRect(ren, &rect[i]);
		SDL_RenderPresent(ren);

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
