#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3];
	int y[3];
	float phi = M_PI / 2;
	for (int i = 0; i < 3; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * M_PI / 3;
	}
	for (int i = 0; i < 3; i++)
		SDL_RenderDrawLine(ren,x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3]);

}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4];
	int y[4];
	float phi = 0;
	for (int i = 0; i < 4; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * M_PI / 4;
	}
	for (int i = 0; i < 4; i++) {
		SDL_RenderDrawLine(ren,x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4]);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float phi = M_PI / 2;
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * M_PI / 5;
    }
	for(int i=0;i<5;i++)
		SDL_RenderDrawLine(ren,x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5]);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6];
	int y[6];
	float phi = M_PI / 2;
	for (int i = 0; i < 6; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * M_PI / 6;
	}
	for (int i = 0; i<6; i++)
		SDL_RenderDrawLine(ren, x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6]);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float phi = M_PI / 2;
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++) {
		SDL_RenderDrawLine(ren,x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5]);
	}
	
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], xp[5];
	int y[5], yp[5];
	float phi = M_PI/2;
	float r = R*sin(M_PI /10 ) / sin(7 * M_PI / 10);
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(r*cos(phi + M_PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(phi + M_PI / 5) + 0.5);
		phi += 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++) {
		SDL_RenderDrawLine(ren,x[i], y[i], xp[i], yp[i]);
		SDL_RenderDrawLine(ren, xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5]);

	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], xp[8];
	int y[8], yp[8];
	float phi = M_PI/2;
	float r = R*sin(M_PI / 8) / sin(6 * M_PI / 8);
	for (int i = 0; i < 8; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(r*cos(phi + M_PI / 8) + 0.5);
		yp[i] = yc - int(r*sin(phi + M_PI / 8) + 0.5);
		phi += 2 * M_PI / 8;
	}
	for (int i = 0; i < 8; i++) {
		SDL_RenderDrawLine(ren,x[i], y[i], xp[i], yp[i]);
	    SDL_RenderDrawLine(ren,xp[i], yp[i], x[(i + 1) % 8], y[(i + 1) % 8]);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], xp[5];
	int y[5], yp[5];
	float phi = startAngle;
	float r = R*sin(M_PI / 10) / sin(7 * M_PI / 10);
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(r*cos(phi + M_PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(phi + M_PI / 5) + 0.5);
		phi += 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++) {
		SDL_RenderDrawLine(ren, x[i], y[i], xp[i], yp[i]);
		SDL_RenderDrawLine(ren, xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5]);

	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float R = r*sin(M_PI / 10) / sin(7 * M_PI / 10);
	DrawStarAngle(xc, yc, r,M_PI/2 ,ren);
	DrawStarAngle(xc, yc, int(R + 0.5),M_PI/2+M_PI, ren);
}
