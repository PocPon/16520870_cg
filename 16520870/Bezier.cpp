#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	Vector2D v;
	for (float j = 0; j <= 1; j = j + 0.001) {
		v.x = (1 - j)*(1 - j)*p1.x + 2 * (1 - j)*j*p2.x + j*j*p3.x;
		v.y = (1 - j)*(1 - j)*p1.y + 2 * (1 - j)*j*p2.y + j*j*p3.y;
		SDL_RenderDrawPoint(ren, v.x, v.y);
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	Vector2D v;
	for (float j = 0; j <= 1; j = j + 0.001) {
		v.x = (1 - j)*(1 - j)*(1 - j)*p1.x + 3 * (1 - j)*(1 - j)*j*p2.x + 3 * (1 - j)*j*j*p3.x + j*j*j*p3.x;
		v.y = (1 - j)*(1 - j)*(1 - j)*p1.y + 3 * (1 - j)*(1 - j)*j*p2.y + 3 * (1 - j)*j*j*p3.y + j*j*j*p3.y;
		SDL_RenderDrawPoint(ren, v.x, v.y);
	}

}
